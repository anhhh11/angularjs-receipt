var Pen = function(position){
  this.position = position;
}
Pen.prototype.move = function(toPostition){
  this.position = position
}
Pen.prototype.select = function(select){
  this.select = select;
}
Pen.prototype.toJSON = function(){
  return {
    position: this.position,
    select: this.select
  }
}

var MagicPen = function(position,acc){
  Pen.call(this,position);
  this.acc  = acc;
}
MagicPen.prototype = Object.create(Pen.prototype);
MagicPen.prototype.constructor = MagicPen;
MagicPen.prototype.toJSON = function(){
  var obj =  Pen.prototype.toJSON.call(this);
  obj.acc = this.acc;
  return obj;
}

var p = new Pen([1,1]);
p.select('todo');
p.toJSON();

var mg = new MagicPen([1,1],[1,2,3]);
