﻿var express = require('express');
var mongoose = require('mongoose');
var app = express();
mongoose.connect('mongodb://localhost/test');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
var Task;
db.once('open', function callback() {
    var taskScheme = mongoose.Schema({
        name: String
    });
    Task = mongoose.model('Task', taskScheme);
});

app.get('/tasks', function (req, res) {
    Task.find(function (err, tasks) {
        res.send(tasks);
    });
});
app.get('/wait', function (req, res) {
    setTimeout(function () { res.send('done');}, 4000);
});
app.use(express.static(__dirname));
app.listen(3000);
console.log('server is running now');