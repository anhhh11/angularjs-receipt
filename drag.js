﻿//dragable: dragstart drag dragend
//dropable: dragenter dragover dragleave drop
//effectAllowed must match dropEffect to allow enter
angular.module('AppA3_Drag', [])
.controller('MainCtrl', function () {

    })
.directive('dragable', function ($log) {
        return {
            link: function (scope, elem, attrs) {
                //Bấm vào đối tượng, di chuyển -> Tạo ra dragstart event mang thông tin của điểm bắt đầu dịch
                //Phải thiết lập dataTransfer thì mới nhận được các event drag, dragend
                elem.on('dragstart', function (e) {
                    $log.log(e);
                    e.dataTransfer.effectAllowed = "move";
                    e.dataTransfer.setData('x', 'Hello world');
                    e.dataTransfer.source = e.target;
                //console.log(e.dataTransfer.getData('x'));
                });
                elem.on('dragover',$log.log)
                elem.on('dragend', function (e) {
                    e.target.remove();
                    console.log('End at start');
                    $log.log(e);
                });

            //Từ điểm đầu di chuyển đối tượng -> event được tạo.
            //elem.on('drag', log);
            //Khi đang di chuyển, thả nút bấm chuột -> event được tạo.
            //elem.on('dragend', log);
            }
        }
    })
.directive('dropable', function ($log) {
        return {
            link: function (scope, elem, attrs) {
                //event xảy ra khi con trỏ chuột đối tượng dropable trỏ vào điểm bất kì của dropable
                elem.on('dragenter', function (event) {
                    $log.log(event);
                    //event.dataTransfer.source.remove();
                });
                //event happend Sau khi đã enter và di chuyển xung quanh dropable
                elem.on('dragover', function (event) {
                    $log.log(event);
                    event.preventDefault();
                    event.dataTransfer.dropEffect = 'move';//move, link, 
                });
                //event xảy ra khi đối tượng dropable đã enter và con trỏ chuột qua ngoài phạm vi của dragable
                elem.on('dragleave', function (e) {
                    $log.log(e);
                });
                elem.on('drop', function (e) {
                    console.log('End at end');
                    $log.log(e);
                });

            }
        }
    });