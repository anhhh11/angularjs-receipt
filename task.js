﻿/*

Provider vs services vs factory
- Khai bao moudle.service('serviceName',function XYZ(){}); thi khi inject new XYZ() se duoc tao va truyen vao (giong kieu dependence injection)
- Khai bao module.factory('facName',function XYZ(){}); thi khi inject XYZ() se duoc truyen vao
- Khai bao module.factory('providerName',function XYZ(){}); thi khi inject XYZ().$get() duoc truyen vao, ngoai ra tai module.config co the inject providerNameProvider va
thuc hien goi cac ham config vd providerNameProvider.setName('xyz').
- Khai bao module.value('valueKey','value') de khai bao gia tri su dung toan cuc
- Khai bao module.constant('constKey','constVal') de khai bao gia tri khai bao 1 lan su dung toan cuc
LUU Y: Khi inject ở config cần thêm Provider vào tên.
*/
/*
Doi voi cac directive built-in cua angular thi no se tu dong goi $digest(hay cat nhat lai view) sau khi model bi sua.
*/
//Trinh tu tao directive: http://jasonmore.net/angular-js-directives-difference-controller-link/
// compile -> prelink -> <child init> -> post
//compile function -> to manipulate template element
//link function -> to add event to element

//$parse -> chuyen expression -> khong thuc hien lenh va chi tra ve function
//scope.$eval -> thuc hien ngay lenh va tra ve ket qua
//http://odetocode.com/blogs/scott/archive/2014/05/28/compile-pre-and-post-linking-in-angularjs.aspx
angular.module('AppA2_Task', ['LocalStorageModule'])
    .config(function (localStorageServiceProvider) {
        localStorageServiceProvider.setPrefix('AppA2_Task');
        localStorageServiceProvider.setStorageCookieDomain('localhost');
    })
 .directive('pendingRequest', function ($http) {
        var pendingRequests = function () {
            return $http.pendingRequests.length;
        };
        return {
            scope: {},
            restrict: 'E',
            template: "<div ng-show='nrOfPendingRequest'> There is #{{nrOfPendingRequest}} remaining</div>",
            controller: function ($scope) {
                $scope.$watch(pendingRequests, function (value) {
                    $scope.nrOfPendingRequest = value;
                });
            },
            controllerAs: 'pr'
        };
    })
.directive('getWait', function ($http) {
        return {
            scope: {},
            restrict: 'E',
            link: function (scope, elem, attrs){
                $http.get('/wait').success(function (val) {
                    elem.html(val);
                });
            }
        }
    })
.directive('a1Dropdownlist', function ($injector) {
        return {
            template: "<select ng-model='selectedValue' ng-options='item.name as item.name for item in items' />",
            scope: {
                valueField : '@',
                textField : '@',
                source: '@',
                selectedValue : '='
            },
            link: function (scope, elem, attr) {
                var sourceObj = scope.source.split('.');
                var service = $injector.get(sourceObj[0]);
                scope.items = service[sourceObj[1]]();
            }
        }
    })
.directive('confirmedClick', function ($compile, $rootScope) {
        return {
            restrict: 'A',
            scope: {
                confirmedClick: '&'
            },
            link: function (scope, element, attrs) {
                scope.displayBox = false;
                element.on('click', function () {
                    element.attr('disabled', 'disabled');
                    //var boxScope = $rootScope.$new(true, scope);//Create isolated and given parent scope
                    //console.log(boxScope);
                    var boxElem = $compile('<div data-confirming-box data-confirm="confirm()" data-is-displayed="displayBox"></div>')(scope);
                    //console.log(boxScope);
                    scope.confirm = function () {
                        scope.confirmedClick();
                        element.removeAttr('disabled');
                    }
                    element.append(boxElem);
                    scope.$apply(function () {
                        scope.displayBox = true;
                    });
                });
            }
        }
    })
.directive('confirmingBox', function () {
        return {
            //require: '^confirmedClick',
            restrict: 'A',
            templateUrl: '_confirm.html',
            scope: {
                confirm: '&',
                isDisplayed: '=',
            },
            link: function (scope, elem, attrs) {
                scope.decide = function (event, execute) {
                    event.stopPropagation();
                    if (execute) {
                        scope.confirm();
                    }
                    elem.remove();
                }
            }
        }
    })
.directive('ngEnter', function () {
        return {
            restrict: 'A',
            scope: {
                model: '=ngModel',
                onEnter: '&ngEnter'
            },
            //replace: true, //If replace is set, then template directive elem is replaced by template
            //template:'<a>Hello</a>',
            //translude:true //Used retain DOM inside directive, ng-translude directive is use to locate where its place.
            controller: function ($scope, $element) {
            },
            link: function (scope, elem, attrs) {
                elem.bind('keydown keypressed', function (e) {
                    if (e.which == 13) //When press ENTER
                    {
                        scope.$apply(function () {
                            scope.onEnter({});
                        });
                    }
                    else if (e.which == 8) //When press BACKSPACE
                    {
                        scope.$apply(function () {
                            scope.model = '';
                        });
                    }
                //e.preventDefault();//Chan cac nut khac
                });
            }
        }
    })
.service('TaskService', function (localStorageService, $rootScope) {
        var $scope = $rootScope.$new();
        $scope.tasks = [];
        $scope.displayBox = true;
        if (!localStorageService.get('tasks')) localStorageService.set('tasks', []);
        localStorageService.bind($scope, 'tasks');
        this.addTask = function (task) {
            $scope.tasks.push(task)
        }
        this.removeTask = function (index) {
            $scope.tasks.splice(index, 1);
        }
        this.getTasks = function () {
            return angular.copy($scope.tasks);
        }
        this.updateTask = function (index, task) {
            $scope.tasks[index] = task;
        }
        this.removeAll = function () {
            localStorageService.set('tasks', []);
        }
    })
.controller('MainController', function ($scope, TaskService) {
        $scope.newTask = function () {
            return { name: $scope.taskName, inEditMode: false };
        }
        $scope.newTaskwithEditable = function () {
            var task = $scope.newTask();
            task.inEditMode = false;
            task.tmpName = '';
            return task;
        }
        $scope.updateAllTask = function () {
            $scope.tasks = TaskService.getTasks();
        }
        $scope.addTask = function () {
            TaskService.addTask($scope.newTask());
            $scope.tasks.push($scope.newTaskwithEditable());
            $scope.taskName = '';
        }
        $scope.removeTask = function (index) {
            TaskService.removeTask(index);
            $scope.tasks.splice(index, 1);
        }
        $scope.removeAll = function () {
            $scope.tasks.splice(0, $scope.tasks.length);
            TaskService.removeAll();
        }
        $scope.taskCount = function () {
            return $scope.tasks.length;
        }
        $scope.hasTask = function () {
            return $scope.taskCount() > 0;
        }
        $scope.editTask = function (index) {
            $scope.tasks[index].inEditMode = true;
        }
        $scope.cancelEdit = function (index) {
            $scope.tasks[index].inEditMode = false;
        }
        $scope.updateTask = function (index) {
            $scope.tasks[index].name = $scope.tasks[index].tmpName;
            TaskService.updateTask(index, $scope.tasks[index]);
            $scope.cancelEdit(index);
        }
        $scope.multipleUpdate = function () {
            for (var i = 0; i < $scope.tasks.length; i++) {
                if ($scope.tasks[i].inEditMode) {
                    $scope.updateTask(i);
                }
            }
        }
        
        $scope.updateAllTask();
        $scope.taskName = '';
        $scope.editedName = '';

    });
