﻿angular.module('AppA2_Task', ['LocalStorageModule'])
    .config(function($interpolateProvider)
    {
        //$interpolateProvider.startSymbol('<%#');
        //$interpolateProvider.endSymbol('%>');
    })
    .config(function (localStorageServiceProvider) {
        localStorageServiceProvider
          .setPrefix('AppA2_Task');
    })
.controller('MainController', function ($scope, $interval, $interpolate,$compile) {
    $scope.z = Date.now();
    $scope.y = $interpolate("hello {{x}}")({ x: $scope.z });
    $scope.k = $compile('<a href="abc.html">{{y}}</a>')($scope).toString();
    $scope.start = false;
    $scope.isLoggined = true;
    $scope.accountType = '';
    $scope.x = {};
    $scope.x.y = {};
    $scope.x.y.z = 10;
    $scope.$watch('x', function () {
        //$scope.x.y.z += 1;//infinite loop
    },true);
    var i;
    //http://www.bennadel.com/blog/2658-using-scope-watch-to-watch-functions-in-angularjs.htm
    //init controller
    //init view
    //init $watch
    //call $digiest
    // loop through all watched to update
    //$apply = execute some code && $digest()
    $scope.decideStart = function () {
        if ($scope.start) {
            i = $interval(function () { $scope.x = Date.now() }, .5);
        }
        else
            $interval.cancel(i);
    }
    $scope.$watch('start', function (n, o) { if(n!==o) console.log(n, o, 1); });
    $scope.$watch('start', function (n, o) { if (n!==o) console.log(n, o, 2); });
    //$ for built-in func
    //Sau khi xuat hien model start trong view, ham $watch duoc tao onfly, voi noi
    //dung dang function(new,old){$("[ng-model='start']") = new}
    //va mac dinh duoc goi voi gia tri ($scope.start,$scope.start) de khoi tao gia tri cho o textbox
    //Bat ki fn cua $watch nao cung bi goi ngay sau khi tao. -> can phai kiem tra dieu kien
    //http://tutorials.jenkov.com/angularjs/watch-digest-apply.html
    //View va Model($scope.<model> su tu cap nhat neu co thay doi tren view model
    //khi thuc hien thao tac bang cac ham hay thuoc tinh dang ng-...
    //trai lai khi thay doi gia tri truc tiep khong thong qua event cua angular -> can $digest hoac $apply sau khi thay doi
});
